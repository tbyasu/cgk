/*
 * Copyright (c) 2018 Yasuo Tabei
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published bytes 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "CGK.hpp"

using namespace std;

double gettimeofday_sec()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec + (double)tv.tv_usec*1e-6;
}


void CGK::readFile(ifstream &ifs, vector<int> &labels, vector<vector<var_t> > &strs) {
  strs.clear();

  sigma_ = 0;
  len_ = 0;
  string line;
  while (getline(ifs, line)) {
    labels.resize(labels.size() + 1);
    strs.resize(strs.size() + 1);
    int &label = labels[labels.size() - 1];
    vector<var_t> &str = strs[strs.size() - 1];

    stringstream ss(line);
    ss >> label;
    string dat;
    ss >> dat;

    for (size_t i = 0; i < dat.size(); ++i) {
      str.push_back(dat[i]);
      if ((uint32_t)dat[i] > sigma_)
	sigma_ = dat[i];
    }
    str.swap(str);
    if (str.size() > len_)
      len_ = str.size();

  }
  sigma_++;
}

void CGK::generateRandVec() {
  srand((unsigned int)time(0));
  uint32_t num = sigma_ * len_ * 3 ;
  randVec_.resize(num);
  for (size_t i = 0; i < num; ++i)
    randVec_[i] = rand() % 2;
}

void CGK::runCGK(std::vector<std::vector<var_t> > &strs) {
  uint32_t len = len_ * 3;
  
  for (size_t i = 0; i < strs.size(); ++i) {
    vector<var_t> &str = strs[i];
    vector<var_t> newstr(len);
    size_t iter = 0;
    for (size_t j = 0; j < len; ++j) {
      if (iter < str.size()) {
	newstr[j] = str[iter];
	//	newstr.push_back(str[iter]);
	iter += randVec_[j * sigma_ + str[iter]];
      }
      else
      	newstr[j] = sigma_;
    }
    strs[i] = newstr;
  }
}

void CGK::expand(vector<vector<var_t> > &strs) {
  fvs_.resize(strs.size());
  for (size_t i = 0; i < strs.size(); ++i) {
    vector<var_t> &str = strs[i];
    vector<uint32_t> &fv = fvs_[i];
    for (size_t j = 0; j < str.size(); ++j) {
      if (str[j] == sigma_)
	break;
      fv.push_back((sigma_ + 2) * j + (str[j] + 1));
    }
  }
}

void CGK::run(const char *inputname, const char *outputname) {
  vector<int> labels;
  vector<vector<var_t> > strs;
  cerr << "readfile: " << inputname << endl;
  {
    ifstream ifs(inputname);
    if (!ifs) {
      cerr << "cannot open: " << inputname << endl;
      exit(1);
    }
    readFile(ifs, labels, strs);
    ifs.close();
  }

  double stime = gettimeofday_sec();
  generateRandVec();
  runCGK(strs);
  expand(strs);
  double etime = gettimeofday_sec();
  cout << "running time (sec) : " << (etime - stime) << endl;

  cout << "mem randvec (bytes): " << randVec_.size()/8 << endl;
  
  cout << "dim: " << fvs_[0].size() << endl;
  
  {
    ofstream ofs(outputname);
    for (size_t i = 0; i < fvs_.size(); ++i) {
      vector<var_t> fv = fvs_[i];
      ofs << labels[i];
      for (size_t j = 0; j < fv.size(); ++j) {
	ofs << " " << fv[j] << ":1";
      }
      ofs << endl;
    }
    
    ofs.close();
  }
}
  
