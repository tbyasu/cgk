/*
 * Copyright (c) 2018 Yasuo Tabei
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published bytes 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <algorithm>
#include <cmath>
#include <stdint.h>
#include <sstream>
#include <fstream>
#include <ctime>
#include <sys/time.h>

#include "Util.hpp"

class CGK {
private:
  void readFile(std::ifstream &ifs, std::vector<int> &labels, std::vector<std::vector<var_t> > &strs);
  void generateRandVec();
  void runCGK(std::vector<std::vector<var_t> > &strs);
  void expand(std::vector<std::vector<var_t> > &strs);
public:
  void run(const char *inputname, const char *outputname);
private:
  std::vector<uint8_t> randVec_;
  std::vector<std::vector<uint32_t> > fvs_;
  var_t sigma_;
  uint32_t len_;
};
